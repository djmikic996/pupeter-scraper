# Etsy scraper

App is done with puppetter, I did it in 2 days.
It has captcha recognition and some type of bypass not the happiest solution, code is written with typescript.
Code is not so reusable because I was in rush, so I made app just to work with required functionality. Better approach fot this type of code, is to write it in oop pattern and then you can make it very reussable and modular.
Tests are witten with json, and they are placed in src/tests, every file hase .test. in filename
Data from scraping is saved in results folder as json format.

## Install

```bash
git clone <REPO_URL>
```

```bash
cd <REPO_URL>
```

### Install dependencies

npm

```bash
npm install
```

yarn

```bash
yarn
```

### Run scripts

Get base information from 10 products:

```bash
yarn start:base
```

Get detailed information from 10 listed above products (visit page on each of them)
Sizes were required in test.pdf but since there are no sizes for most objects or they are represented as the variants, I made variants property with listed options.
Before this you need to run `yarn start:base`, to get basic data from products including url's

```bash
yarn start:visit
```

Add to shopping cart and checkout simulation for one product:

```bash
yarn start:checkout
```

### Test

If you have problem with timeouts feel free to change them, because they can depend of the internet or device speed.

```bash
yarn test <fileName.test.ts>
```

### Format files:

```bash
yarn format
```
