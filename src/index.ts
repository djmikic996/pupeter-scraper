import fs from 'fs';
import { join } from 'path';

import { ProductBaseType } from './types/ProductBase.type';
import { checkIsCaptcha } from './utils/checkIsCaptcha';
import { BaseBrowser } from './browser/BaseBrowser';

(async () => {
  const browser = new BaseBrowser();
  await browser.applySettings(true);
  const p = await browser.page;

  await p.goto('https://www.etsy.com/uk/featured/hub/best-home-deals?ref=SHRS24_cat_nav');

  await new Promise((resolve) => setTimeout(resolve, 5000));

  const captcha = await p.evaluate(checkIsCaptcha);

  if (captcha)
    await p.goto(
      'https://www.etsy.com/uk/featured/hub/best-home-deals?ref=SHRS24_cat_nav',
    );

  await p.waitForSelector('a.wt-btn.wt-btn--tertiary');
  await p.click('a.wt-btn.wt-btn--tertiary');
  await p.waitForNavigation({ waitUntil: 'networkidle2' });

  const redirectedURL = p.url();

  console.log('Redirected URL:', redirectedURL);

  await p.goto(redirectedURL);

  await p.waitForSelector('#content');

  // Extract data
  const el = await p.evaluate((): ProductBaseType[] => {
    const link = Array.from(
      document.querySelectorAll('.js-merch-stash-check-listing.v2-listing-card'),
    ).slice(0, 10);
    const data: ProductBaseType[] = [];

    link.forEach((item: Element, index: number) => {
      data.push({
        id: index,
        link: item?.querySelector('a')?.href || '',
        name: item?.querySelector('h3')?.innerHTML.trim() || '',
        price:
          item?.querySelector('.currency-value')?.innerHTML +
            ' ' +
            item?.querySelector('.currency-symbol')?.innerHTML || '',
      });
    });

    return data;
  });

  console.log(el);

  // Write to file
  fs.writeFileSync(
    join(__dirname, '..', 'results', 'products.json'),
    JSON.stringify(el, null, 2),
  );

  await browser.closeBrowser();
})();
