import fs from 'fs';
import { join } from 'path';

import products from '../results/products.json';
import { ProductDetailType } from './types/ProductDetail.type';
import { BaseBrowser } from './browser/BaseBrowser';

(async () => {
  const browser = new BaseBrowser();
  await browser.applySettings();
  const p = await browser.page;

  const data: ProductDetailType[] = [];

  for (const product of products) {
    console.log(product.link);
    await p.goto(product.link);

    await new Promise((resolve) => setTimeout(resolve, 5000));
    await p.waitForSelector('#content', { timeout: 10000 });

    // Extract data
    const result = await p.evaluate(() => {
      const name =
        document.querySelector('p.wt-text-title-01.wt-text-brick.appears-ready')
          ?.textContent || '';
      const price =
        document
          .querySelector('p.wt-text-title-larger.wt-mr-xs-1.wt-text-slime')
          ?.textContent?.trim()
          ?.split(' ')
          ?.at(-1)
          ?.replace('+', '') || '';
      const desc =
        document
          .querySelector(
            'h1.wt-text-body-01.wt-line-height-tight.wt-break-word.wt-mt-xs-1',
          )
          ?.textContent?.trim() || '';
      const variants: string[] = [];
      const img =
        (
          document.querySelector(
            '.wt-position-absolute.wt-width-full.wt-height-full.wt-position-top.wt-position-left.carousel-pane img',
          ) as HTMLImageElement
        )?.src || '';

      if (document.getElementById('variation-selector-0')) {
        const options = Array.from(
          (document.getElementById('variation-selector-0') as HTMLSelectElement)?.options,
        );

        if (options?.length) {
          console.log(options);
          options.forEach((e: HTMLOptionElement) =>
            variants.push(e?.textContent?.trim() || ''),
          );
        }
      }

      console.log({
        name,
        price,
        desc,
        variants: variants.slice(1),
        img,
      });

      return {
        name,
        price,
        desc,
        variants: variants.slice(1),
        img,
      };
    });

    data.push(result);

    console.log(result);
  }

  // Write to file
  fs.writeFileSync(
    join(__dirname, '..', 'results', 'detailProducts.json'),
    JSON.stringify(data, null, 2),
  );

  console.log(data, 'Data');

  await browser.closeBrowser();
})();
