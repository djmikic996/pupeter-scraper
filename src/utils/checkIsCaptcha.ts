export const checkIsCaptcha = (): boolean => {
  if (!document.querySelector('iframe')!.src.match(/captcha/gi)) return false;

  return true;
};
