export interface ProductBaseType {
  name: string;
  price: string;
  id?: number;
  link?: string;
}
