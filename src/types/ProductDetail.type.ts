import { ProductBaseType } from './ProductBase.type';

export interface ProductDetailType extends ProductBaseType {
  desc: string;
  variants: string[];
  img: string;
}
