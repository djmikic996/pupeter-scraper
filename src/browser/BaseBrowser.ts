import { Browser, Page, executablePath } from 'puppeteer';
import puppeteerExtra from 'puppeteer-extra';
import randomAgent from 'random-useragent';
import Stealth from 'puppeteer-extra-plugin-stealth';

import { USER_AGENT } from '../constants';

export class BaseBrowser {
  private browser!: Browser;
  private p!: Page;
  private agent: string | null;
  private UA: string;

  constructor() {
    puppeteerExtra.use(Stealth());
    this.agent = randomAgent.getRandom();
    this.UA = this.agent || USER_AGENT;
  }

  async setBrowser(headless: boolean = false) {
    this.browser = await puppeteerExtra.launch({
      headless,
      executablePath: executablePath(),
      args: ['--no-sandbox'],
    });
  }

  async setPage() {
    this.p = await this.browser.newPage();
    await this.p.setViewport({
      width: 1920 + Math.floor(Math.random() * 100),
      height: 3000 + Math.floor(Math.random() * 100),
      deviceScaleFactor: 1,
      hasTouch: false,
      isLandscape: false,
      isMobile: false,
    });
    await this.p.setUserAgent('mozikka' || this.UA);
    await this.p.setJavaScriptEnabled(true);
    await this.p.setDefaultNavigationTimeout(0);
  }

  async applySettings(headless?: boolean) {
    await this.setBrowser(headless);
    await this.setPage();
  }

  async closeBrowser() {
    await this.browser.close();
  }

  get page() {
    return this.p;
  }
}
