import puppeteer from 'puppeteer-extra';
import randomUseragent from 'random-useragent';
import Stealth from 'puppeteer-extra-plugin-stealth';

import { checkIsCaptcha } from '../utils/checkIsCaptcha';
import { USER_AGENT } from '../constants';

describe('Checkout Process', () => {
  let browser;
  let page;

  beforeAll(async () => {
    puppeteer.use(Stealth());
    browser = await puppeteer.launch({ headless: false, args: ['--no-sandbox'] });
  });

  beforeEach(async () => {
    page = await browser.newPage();
  });

  afterEach(async () => {
    await page.close();
  });

  afterAll(async () => {
    await browser.close();
  });

  test('Checkout process', async () => {
    const userAgent = randomUseragent.getRandom();
    await page.setUserAgent(userAgent || USER_AGENT);
    await page.setViewport({ width: 1920, height: 1080 });
    await page.setJavaScriptEnabled(true);
    await page.setDefaultNavigationTimeout(0);

    await page.goto(
      'https://www.etsy.com/uk/listing/1017319117/designer-ceramic-bottle-for-water-or',
    );

    await new Promise((resolve) => setTimeout(resolve, 5000));

    const captcha = await page.evaluate(checkIsCaptcha);

    if (captcha)
      await page.goto(
        'https://www.etsy.com/uk/listing/1017319117/designer-ceramic-bottle-for-water-or',
      );

    await page.waitForSelector('#content', { timeout: 10000 });

    const textarea = await page.$('textarea.wt-textarea.wt-textarea');
    await textarea?.type('Some name');

    const submitBtn = await page.$('button.wt-btn.wt-btn--filled.wt-width-full');
    expect(submitBtn).toBeTruthy();
    await submitBtn?.click();

    await page.waitForNavigation({ waitUntil: 'networkidle2' });

    const redirectedURL = page.url();
    console.log('Redirected URL:', redirectedURL);
    expect(redirectedURL).toBe(page.url());

    const checkoutBtn = await page.$(
      'button.proceed-to-checkout.wt-btn.wt-btn--filled.wt-mt-xs-2.wt-width-full.inline-overlay-trigger.guest-checkout-action',
    );
    expect(checkoutBtn).toBeTruthy();
    await checkoutBtn?.click();

    const modalCheckout = await page.waitForSelector(
      '.wt-overlay__modal.wt-overlay--animation-done',
      {
        timeout: 10000,
      },
    );

    expect(modalCheckout).toBeTruthy();

    console.log('Checked out');
  }, 30000);
});
