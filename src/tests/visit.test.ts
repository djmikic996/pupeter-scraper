import path from 'path';
import fs from 'fs';
import { exec } from 'child_process';

describe('Scrape and Write detail of Products to JSON', () => {
  test('Scrape and write products with details to JSON file and check are 10 products written to json file', (done) => {
    const command = 'yarn start:visit';
    exec(command, (error, stdout, stderr) => {
      if (error) {
        console.error(`exec error: ${error}`);
        done(error);
        return;
      }

      console.log('stdout:', stdout);
      console.error('stderr:', stderr);

      const filePath = path.join(__dirname, '..', '..', 'results', 'detailProducts.json');
      const fileExists = fs.existsSync(filePath);
      expect(fileExists).toBe(true);

      const products = fs.readFileSync(filePath, 'utf8');

      expect(JSON.parse(products).length).toBe(10);
      done();
    });
  }, 100000);
});
