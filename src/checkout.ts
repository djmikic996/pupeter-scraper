import { checkIsCaptcha } from './utils/checkIsCaptcha';
import { BaseBrowser } from './browser/BaseBrowser';

(async () => {
  const browser = new BaseBrowser();
  await browser.applySettings();
  const p = await browser.page;

  await p.goto(
    'https://www.etsy.com/uk/listing/1017319117/designer-ceramic-bottle-for-water-or',
  );

  await new Promise((resolve) => setTimeout(resolve, 5000));

  const captcha = await p.evaluate(checkIsCaptcha);

  if (captcha)
    await p.goto(
      'https://www.etsy.com/uk/listing/1017319117/designer-ceramic-bottle-for-water-or',
    );

  await p.waitForSelector('#content', { timeout: 10000 });

  const tarea = await p.$('textarea.wt-textarea.wt-textarea');
  await tarea?.type('Some name');

  const submitBtn = await p.$('button.wt-btn.wt-btn--filled.wt-width-full');
  await submitBtn?.click();

  await p.waitForNavigation({ waitUntil: 'networkidle2' });

  const redirectedURL = p.url();

  console.log('Redirected URL:', redirectedURL);

  const checkoutBtn = await p.$(
    'button.proceed-to-checkout.wt-btn.wt-btn--filled.wt-mt-xs-2.wt-width-full.inline-overlay-trigger.guest-checkout-action',
  );
  await checkoutBtn?.click();

  await p.waitForSelector('.wt-overlay__modal.wt-overlay--animation-done', {
    timeout: 10000,
  });

  console.log('Checked out');

  await browser.closeBrowser();
})();
